﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RealmFarerWebsite.Controllers.Data;
using RealmFarerWebsite.Services;

namespace RealmFarerWebsite.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class RegisterController : Controller
    {
        private readonly IRegisterService _registerService;

        public RegisterController(IRegisterService registerService)
        {
            _registerService = registerService;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult>Post([FromBody] RegistrationData registrationData)
        {
            try
            {
                var registrationResult = await _registerService.TryRegisterNewUserAsync(registrationData);

                return base.Ok(registrationResult);
            }
            catch (Exception)
            {
                return base.StatusCode(500);
            }
        }
    }
}
