﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RealmFarerWebsite.Controllers.Data;
using RealmFarerWebsite.Exceptions;
using RealmFarerWebsite.Services;

namespace RealmFarerWebsite.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class LoginController : Controller
    {
        private readonly ILoginService _loginService;

        public LoginController(ILoginService loginService)
        {
            _loginService = loginService;
        }

        [AllowAnonymous]
        [HttpPost(nameof(Authenticate))]
        public async Task<IActionResult> Authenticate([FromBody] LoginCredentials loginCredentials)
        {
            try
            {
                var authenticatedUser = await _loginService.AuthenticateCredentialsOrThrowAsync(loginCredentials);

                return base.Ok(authenticatedUser);
            }
            catch(LoginCredentialsInvalidException)
            {
                return base.UnprocessableEntity();
            }
            catch (Exception)
            {
                return base.StatusCode(500);
            }
        }
    }
}
