﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RealmFarerWebsite.Persistence.Models;
using RealmFarerWebsite.Persistence.Repositories;
using RealmFarerWebsite.ViewModels;

namespace RealmFarerWebsite.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ForumPostController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IForumPostRepository _forumPostRepository;

        public ForumPostController(
            IMapper mapper,
            IForumPostRepository forumPostRepository)
        {
            _mapper = mapper;
            _forumPostRepository = forumPostRepository;
        }

        [HttpGet(nameof(GetPosts))]
        public async Task<IActionResult> GetPosts(long forumTopicId)
        {
            try
            {
                var forumPosts = await _forumPostRepository.GetForumPostsOfAsync(forumTopicId);

                var forumPostViewModels = _mapper.Map<IEnumerable<ForumPostViewModel>>(forumPosts);

                return base.Ok(forumPostViewModels);
            }
            catch (Exception)
            {
                return base.StatusCode(500);
            }
        }

        [HttpPost(nameof(PostForumPost))]
        public async Task<IActionResult> PostForumPost([FromBody] ForumPostViewModel forumPostViewModel)
        {
            try
            {
                var forumPost = _mapper.Map<ForumPost>(forumPostViewModel);

                await _forumPostRepository.CreateForumPostAsync(forumPost);

                return base.Ok();
            }
            catch (Exception)
            {
                return base.StatusCode(500);
            }
        }

    }
}
