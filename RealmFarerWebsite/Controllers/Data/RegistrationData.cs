﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Controllers.Data
{
    public class RegistrationData
    {
        public string Username { get; set; }

        public string PlainTextPassword { get; set; }

        // Can also include more information, such as security questions
    }
}
