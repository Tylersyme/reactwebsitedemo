﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Controllers.Data
{
    public class AuthenticatedUser
    {
        public string Token { get; set; }

        public long UserId { get; set; }
    }
}
