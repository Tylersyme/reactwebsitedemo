﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Controllers.Data
{
    public class RegistrationResult
    {
        public bool WasSuccessful { get; set; }
    }
}
