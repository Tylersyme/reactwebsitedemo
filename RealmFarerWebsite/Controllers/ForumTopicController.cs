﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RealmFarerWebsite.Persistence.Models;
using RealmFarerWebsite.Persistence.Repositories;
using RealmFarerWebsite.ViewModels;

namespace RealmFarerWebsite.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ForumTopicController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IForumTopicRepository _forumTopicRepository;

        public ForumTopicController(
            IMapper mapper,
            IForumTopicRepository forumTopicRepository)
        {
            _mapper = mapper;
            _forumTopicRepository = forumTopicRepository;
        }

        [HttpGet(nameof(GetTopics))]
        public async Task<IActionResult> GetTopics(long forumId)
        {
            try
            {
                var forumTopics = await _forumTopicRepository.GetForumTopicsOfAsync(forumId);

                var forumTopicViewModels = _mapper.Map<IEnumerable<ForumTopicViewModel>>(forumTopics);

                return base.Ok(forumTopicViewModels);
            }
            catch (Exception)
            {
                return base.StatusCode(500);
            }
        }

        [HttpPost(nameof(PostForumTopic))]
        public async Task<IActionResult> PostForumTopic([FromBody] ForumTopicViewModel forumTopicViewModel)
        {
            try
            {
                var forumTopic = _mapper.Map<ForumTopic>(forumTopicViewModel);

                await _forumTopicRepository.CreateForumTopicAsync(forumTopic);

                return base.CreatedAtAction(nameof(PostForumTopic), new { forumTopic.Id });
            }
            catch (Exception)
            {
                return base.StatusCode(500);
            }
        }
    }
}
