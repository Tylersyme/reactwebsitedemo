﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RealmFarerWebsite.Persistence.Models;
using RealmFarerWebsite.Persistence.Repositories;
using RealmFarerWebsite.ViewModels;

namespace RealmFarerWebsite.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ForumController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IForumRepository _forumRepository;

        public ForumController(
            IMapper mapper, 
            IForumRepository forumRepository)
        {
            _mapper = mapper;
            _forumRepository = forumRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var forums = await _forumRepository.GetAllAsync();

                var forumViewModels = _mapper.Map<IEnumerable<ForumViewModel>>(forums);

                return base.Ok(forumViewModels);
            }
            catch (Exception)
            {
                return base.StatusCode(500);
            }
        }
    }
}
