﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite
{
    public static class MappingConfiguration
    {
        public static IMapper Mapper;

        public static void ConfigureMappings()
        {
            var assembly = typeof(MappingConfiguration).Assembly;

            Mapper = new MapperConfiguration(cfg => cfg.AddMaps(assembly)).CreateMapper();
        }
    }
}
