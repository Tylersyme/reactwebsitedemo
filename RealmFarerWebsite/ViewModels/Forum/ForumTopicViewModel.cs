﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.ViewModels
{
    public class ForumTopicViewModel
    {
        public long Id { get; set; }

        public long ForumId { get; set; }

        public long UserId { get; set; }

        public string Subject { get; set; }

        public string Username { get; set; }

        public int TotalPosts { get; set; }
    }
}
