﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.ViewModels
{
    public class ForumViewModel
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int TotalPosts { get; set; }

        public int TotalTopics { get; set; }
    }
}
