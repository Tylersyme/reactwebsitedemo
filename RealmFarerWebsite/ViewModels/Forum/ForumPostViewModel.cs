﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.ViewModels
{
    public class ForumPostViewModel
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        public long ForumTopicId { get; set; }

        public string Username { get; set; }

        public string Content { get; set; }
    }
}
