﻿using AutoMapper;
using RealmFarerWebsite.Persistence.Models;
using RealmFarerWebsite.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.MappingProfiles
{
    public class ForumTopicProfile : Profile
    {
        public ForumTopicProfile()
        {
            base.CreateMap<ForumTopic, ForumTopicViewModel>()
                .ForMember(dest => dest.Username, opts => opts.MapFrom(src => src.User.Username))
                .ForMember(dest => dest.TotalPosts, opts => opts.MapFrom(src => src.ForumPosts.Count()));

            base.CreateMap<ForumTopicViewModel, ForumTopic>();
        }
    }
}
