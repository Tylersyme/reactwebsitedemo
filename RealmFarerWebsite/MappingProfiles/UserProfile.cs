﻿using AutoMapper;
using RealmFarerWebsite.Controllers.Data;
using RealmFarerWebsite.Persistence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            base.CreateMap<RegistrationData, User>();
        }
    }
}
