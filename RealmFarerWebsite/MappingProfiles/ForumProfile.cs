﻿using AutoMapper;
using RealmFarerWebsite.Persistence.Models;
using RealmFarerWebsite.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.MappingProfiles
{
    public class ForumProfile : Profile
    {
        public ForumProfile()
        {
            base.CreateMap<Forum, ForumViewModel>();
            base.CreateMap<ForumViewModel, Forum>();
        }
    }
}
