﻿using AutoMapper;
using RealmFarerWebsite.Persistence.Models;
using RealmFarerWebsite.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.MappingProfiles
{
    public class ForumPostProfile : Profile
    {
        public ForumPostProfile()
        {
            base.CreateMap<ForumPost, ForumPostViewModel>()
                .ForMember(dest => dest.Username, opts => opts.MapFrom(src => src.User.Username));

            base.CreateMap<ForumPostViewModel, ForumPost>();
        }
    }
}
