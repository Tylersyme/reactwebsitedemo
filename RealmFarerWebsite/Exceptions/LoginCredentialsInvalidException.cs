﻿using RealmFarerWebsite.Controllers.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Exceptions
{
    public class LoginCredentialsInvalidException : Exception
    {
    }
}
