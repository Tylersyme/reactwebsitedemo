﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RealmFarerWebsite.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Forums",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Forums", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Username = table.Column<string>(maxLength: 20, nullable: false),
                    HashedPassword = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ForumTopics",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ForumId = table.Column<long>(nullable: false),
                    UserId = table.Column<long>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Subject = table.Column<string>(maxLength: 128, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ForumTopics", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ForumTopics_Forums_ForumId",
                        column: x => x.ForumId,
                        principalTable: "Forums",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ForumTopics_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ForumPosts",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDelete = table.Column<bool>(nullable: false),
                    ForumTopicId = table.Column<long>(nullable: false),
                    UserId = table.Column<long>(nullable: false),
                    Content = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ForumPosts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ForumPosts_ForumTopics_ForumTopicId",
                        column: x => x.ForumTopicId,
                        principalTable: "ForumTopics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ForumPosts_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "Forums",
                columns: new[] { "Id", "Description", "IsDeleted", "Title" },
                values: new object[,]
                {
                    { 1L, "Talk about all things Realm Farer", false, "General Discussion" },
                    { 2L, "Submit bug reports for Realm Farer", false, "Bug Reports" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "HashedPassword", "IsDeleted", "Username" },
                values: new object[,]
                {
                    { 1L, "AQAAAAEAACcQAAAAEOkGNc6mRBYI0EE+9V1AfDU8q/52VMhBAbxeXgX1ucGCVgEZC8SvonjIrxLFvFr5GA==", false, "Exonto" },
                    { 2L, "AQAAAAEAACcQAAAAEMgWz1jK9DivD5vL8VF9OctkK+NEPj4aYM1OX0YqI0AgtiwY8XDrem/zQfLxXJROpQ==", false, "Musafa" },
                    { 3L, "AQAAAAEAACcQAAAAEDdO8gjioIftsZfY/7Byq9zlfKieX+TXHgSe8rJsv1BRKq51kP+UbYHDdMvSJctFaw==", false, "Heffertripe0" },
                    { 4L, "AQAAAAEAACcQAAAAECTVrMObhwANwDi5CO5PURGRalqd6r7H/6oE5HqlHP8jgTIl9SfrQ/HlVENfRoA4JA==", false, "Original_Rando" },
                    { 5L, "AQAAAAEAACcQAAAAEC6gTcSiPZ46SUs0ywj3dP6jVN/rhEGAU6WobqZcM0c7pkpbKA3FoyswBMTAEVmFSQ==", false, "TheTrueBob23" },
                    { 6L, "AQAAAAEAACcQAAAAEEmvMLioT2BXhkt8DvyPADpp0MKPYOJylwouYPmJOWzCZugg7mBPYnazQKDle6WSzA==", false, "McStickins" }
                });

            migrationBuilder.InsertData(
                table: "ForumTopics",
                columns: new[] { "Id", "ForumId", "IsDeleted", "Subject", "UserId" },
                values: new object[,]
                {
                    { 1L, 1L, false, "Can you eat cats in this game?", 3L },
                    { 2L, 1L, false, "You can skate on the ice with your backpack", 4L },
                    { 3L, 2L, false, "Gravity fails when opening door", 5L },
                    { 4L, 2L, false, "Grass turns pink after sleeping on floor", 6L }
                });

            migrationBuilder.InsertData(
                table: "ForumPosts",
                columns: new[] { "Id", "Content", "ForumTopicId", "IsDelete", "UserId" },
                values: new object[,]
                {
                    { 1L, "You can, but in most cultures that is looked down upon. People's opinion of you will probably drop.", 1L, false, 6L },
                    { 2L, "Can confirm. Ate cats. Got put on trial and hung for murder.", 1L, false, 5L },
                    { 3L, "Now if I can only get my ox to pull me", 2L, false, 4L },
                    { 4L, "Whenever I open a door, my character can float over obstacles...", 3L, false, 3L },
                    { 5L, "That doesn't happen on my machine", 3L, false, 1L },
                    { 6L, "Whenever I sleep on the floor without any blanket or pillows, I wake up to the grass being colored pink?!", 4L, false, 2L },
                    { 7L, "That means your character contracted pink eye. Can you confirm whether this is true?", 4L, false, 1L }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ForumPosts_ForumTopicId",
                table: "ForumPosts",
                column: "ForumTopicId");

            migrationBuilder.CreateIndex(
                name: "IX_ForumPosts_UserId",
                table: "ForumPosts",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ForumTopics_ForumId",
                table: "ForumTopics",
                column: "ForumId");

            migrationBuilder.CreateIndex(
                name: "IX_ForumTopics_UserId",
                table: "ForumTopics",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Username",
                table: "Users",
                column: "Username",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ForumPosts");

            migrationBuilder.DropTable(
                name: "ForumTopics");

            migrationBuilder.DropTable(
                name: "Forums");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
