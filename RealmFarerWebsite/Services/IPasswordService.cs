﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Services
{
    public interface IPasswordService
    {
        string Hash(string plainTextPassword);

        bool Verify(string hashedPassword, string password);
    }
}
