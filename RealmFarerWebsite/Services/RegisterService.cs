﻿using AutoMapper;
using RealmFarerWebsite.Controllers.Data;
using RealmFarerWebsite.Persistence.Models;
using RealmFarerWebsite.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Services
{
    public class RegisterService : IRegisterService
    {
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;
        private readonly IPasswordService _passwordService;

        public RegisterService(
            IMapper mapper,
            IUserRepository userRepository,
            IPasswordService passwordService)
        {
            _mapper = mapper;
            _userRepository = userRepository;
            _passwordService = passwordService;
        }

        public async Task<RegistrationResult> TryRegisterNewUserAsync(RegistrationData registrationData)
        {
            var isRegistrationDataValid = await this.IsRegistrationDataValidAsync(registrationData);

            if (!isRegistrationDataValid)
                return new RegistrationResult
                {
                    WasSuccessful = false,
                };

            var user = _mapper.Map<User>(registrationData);
            user.HashedPassword = _passwordService.Hash(registrationData.PlainTextPassword);

            await _userRepository.CreateUserAsync(user);

            return new RegistrationResult
            {
                WasSuccessful = true,
            };
        }

        private async Task<bool> IsRegistrationDataValidAsync(RegistrationData registrationData)
        {
            if (registrationData.PlainTextPassword.Length < 6)
                return false;

            return !(await _userRepository.UserWithUsernameExistsAsync(registrationData.Username));
        }
    }
}
