﻿using CryptoHelper;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using RealmFarerWebsite.Controllers.Data;
using RealmFarerWebsite.Exceptions;
using RealmFarerWebsite.Persistence.Models;
using RealmFarerWebsite.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Services
{
    public class LoginService : ILoginService
    {
        private readonly IPasswordService _passwordService;
        private readonly IUserRepository _userRepository;
        private readonly AppSettings _appSettings;

        public LoginService(
            IPasswordService passwordService,
            IUserRepository userRepository,
            IOptions<AppSettings> appSettings)
        {
            _passwordService = passwordService;
            _userRepository = userRepository;
            _appSettings = appSettings.Value;
        }

        public async Task<AuthenticatedUser> AuthenticateCredentialsOrThrowAsync(LoginCredentials loginCredentials)
        {
            var user = await _userRepository.GetByUsernameAsync(loginCredentials.Username);

            if (user == null)
                throw new LoginCredentialsInvalidException();

            var isPasswordValid = _passwordService.Verify(user.HashedPassword, loginCredentials.PlainTextPassword);

            if (!isPasswordValid)
                throw new LoginCredentialsInvalidException();

            var tokenHandler = new JwtSecurityTokenHandler();
            var secretKeyBytes = Encoding.ASCII.GetBytes(_appSettings.SecretKey);

            var securityTokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                }),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = new SigningCredentials
                    (
                        new SymmetricSecurityKey(secretKeyBytes), 
                        SecurityAlgorithms.HmacSha256Signature
                    ),
            };

            var token = tokenHandler.CreateToken(securityTokenDescriptor);

            return new AuthenticatedUser
            {
                Token = tokenHandler.WriteToken(token),
                UserId = user.Id,
            };
        }
    }
}
