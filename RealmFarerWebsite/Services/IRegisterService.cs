﻿using RealmFarerWebsite.Controllers.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Services
{
    public interface IRegisterService
    {
        Task<RegistrationResult> TryRegisterNewUserAsync(RegistrationData registrationData);
    }
}
