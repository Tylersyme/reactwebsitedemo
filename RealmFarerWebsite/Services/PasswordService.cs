﻿using CryptoHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Services
{
    public class PasswordService : IPasswordService
    {
        public string Hash(string plainTextPassword) =>
            Crypto.HashPassword(plainTextPassword);

        public bool Verify(string hashedPassword, string plainTextPassword) =>
            Crypto.VerifyHashedPassword(hashedPassword, plainTextPassword);
    }
}
