﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Persistence.Models
{
    public class ForumPost
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public bool IsDelete { get; set; }

        [ForeignKey(nameof(Models.ForumTopic))]
        public long ForumTopicId { get; set; }
        public virtual ForumTopic ForumTopic { get; set; }

        [ForeignKey(nameof(User))]
        public long UserId { get; set; }
        public virtual User User { get; set; }

        public string Content { get; set; }
    }
}
