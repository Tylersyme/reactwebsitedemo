﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Persistence.Models
{
    public class Forum
    {
        public Forum()
        {
            this.ForumTopics = new List<ForumTopic>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public bool IsDeleted { get; set; }

        [MaxLength(128)]
        [Required]
        public string Title { get; set; }

        [MaxLength(128)]
        [Required]
        public string Description { get; set; }

        public virtual IEnumerable<ForumTopic> ForumTopics { get; set; }
    }
}
