﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Persistence.Models
{
    public class User
    {
        public User()
        {
            this.ForumTopics = new List<ForumTopic>();
            this.ForumPosts = new List<ForumPost>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public bool IsDeleted { get; set; }

        [Required]
        [MaxLength(20)]
        public string Username { get; set; }

        // TODO: Store as byte[]
        [Required]
        public string HashedPassword { get; set; }

        public virtual IEnumerable<ForumTopic> ForumTopics { get; set; }

        public virtual IEnumerable<ForumPost> ForumPosts { get; set; }
    }
}
