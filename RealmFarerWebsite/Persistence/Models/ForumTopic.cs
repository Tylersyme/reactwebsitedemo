﻿using RealmFarerWebsite.Persistence.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Persistence.Models
{
    public class ForumTopic
    {
        public ForumTopic()
        {
            this.ForumPosts = new List<ForumPost>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [ForeignKey(nameof(Models.Forum))]
        public long ForumId { get; set; }
        public virtual Forum Forum { get; set; }

        [ForeignKey(nameof(User))]
        public long UserId { get; set; }
        public virtual User User { get; set; }

        public bool IsDeleted { get; set; }

        [MaxLength(128)]
        [Required]
        public string Subject { get; set; }

        public virtual IEnumerable<ForumPost> ForumPosts { get; set; }
    }
}
