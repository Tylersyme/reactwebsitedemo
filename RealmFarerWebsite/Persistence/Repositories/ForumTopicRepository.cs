﻿using Microsoft.EntityFrameworkCore;
using RealmFarerWebsite.Persistence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Persistence.Repositories
{
    public class ForumTopicRepository : IForumTopicRepository
    {
        private readonly DataContext _dataContext;

        public ForumTopicRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<ForumTopic>> GetForumTopicsOfAsync(long forumId) =>
            await _dataContext
                .Set<ForumTopic>()
                .Where(ft => !ft.IsDeleted &&
                              ft.ForumId == forumId)
                .Include(f => f.User)
                .Include(f => f.ForumPosts)
                .ToListAsync();

        public async Task CreateForumTopicAsync(ForumTopic forumTopic)
        {
            _dataContext
                .Set<ForumTopic>()
                .Add(forumTopic);

            await _dataContext.SaveChangesAsync();
        }
    }
}
