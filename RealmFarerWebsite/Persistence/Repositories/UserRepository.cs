﻿using Microsoft.EntityFrameworkCore;
using RealmFarerWebsite.Persistence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Persistence.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _dataContext;

        public UserRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<User> GetByUsernameAsync(string username) =>
            await _dataContext
                .Set<User>()
                .FirstOrDefaultAsync(u => !u.IsDeleted &&
                                           u.Username == username);

        public async Task<bool> UserWithUsernameExistsAsync(string username) =>
            await _dataContext
                .Set<User>()
                .AnyAsync(u => !u.IsDeleted &&
                                u.Username == username);

        public async Task CreateUserAsync(User user)
        {
            _dataContext
                .Set<User>()
                .Add(user);

            await _dataContext.SaveChangesAsync();
        }
    }
}
