﻿using Microsoft.EntityFrameworkCore;
using RealmFarerWebsite.Persistence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Persistence.Repositories
{
    public class ForumPostRepository : IForumPostRepository
    {
        private readonly DataContext _dataContext;

        public ForumPostRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<ForumPost>> GetForumPostsOfAsync(long forumTopicId) =>
            await _dataContext
                .Set<ForumPost>()
                .Where(fp => !fp.IsDelete &&
                              fp.ForumTopicId == forumTopicId)
                .Include(fp => fp.User)
                .ToListAsync();

        public async Task CreateForumPostAsync(ForumPost forumPost)
        {
            _dataContext
                .Set<ForumPost>()
                .Add(forumPost);

            await _dataContext.SaveChangesAsync();
        }

    }
}
