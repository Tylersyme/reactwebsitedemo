﻿using Microsoft.EntityFrameworkCore;
using RealmFarerWebsite.Persistence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Persistence.Repositories
{
    public class ForumRepository : IForumRepository
    {
        private readonly DataContext _dataContext;

        public ForumRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<Forum>> GetAllAsync() =>
            await _dataContext
                .Set<Forum>()
                .Where(f => !f.IsDeleted)
                .ToListAsync();
    }
}
