﻿using RealmFarerWebsite.Persistence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Persistence.Repositories
{
    public interface IForumTopicRepository
    {
        Task<IEnumerable<ForumTopic>> GetForumTopicsOfAsync(long forumId);
        Task CreateForumTopicAsync(ForumTopic forumTopic);
    }
}
