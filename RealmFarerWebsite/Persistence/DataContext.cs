﻿using Microsoft.EntityFrameworkCore;
using RealmFarerWebsite.Persistence.Models;
using RealmFarerWebsite.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealmFarerWebsite.Persistence
{
    public class DataContext : DbContext
    {
        private readonly IPasswordService _passwordService;

        public DataContext(
            DbContextOptions dbContextOptions,
            IPasswordService passwordService)
            : base(dbContextOptions)
        {
            _passwordService = passwordService;
        }

        public DbSet<Forum> Forums { get; set; }

        public DbSet<ForumTopic> ForumTopics { get; set; }

        public DbSet<ForumPost> ForumPosts { get; set; }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<User>()
                .HasIndex(u => u.Username)
                .IsUnique();

            // Users should never be hard deleted but sql server requires that
            // cyclic cascade deletions be impossible
            modelBuilder
                .Entity<ForumPost>()
                .HasOne(fp => fp.User)
                .WithMany(u => u.ForumPosts)
                .HasForeignKey(fp => fp.UserId)
                .OnDelete(DeleteBehavior.NoAction);

            this.Seed(modelBuilder);
        }

        private void Seed(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Forum>()
                .HasData(
                    new Forum
                    {
                        Id = 1,
                        IsDeleted = false,
                        Title = "General Discussion",
                        Description = "Talk about all things Realm Farer",
                    },
                    new Forum
                    {
                        Id = 2,
                        IsDeleted = false,
                        Title = "Bug Reports",
                        Description = "Submit bug reports for Realm Farer",
                    });

            modelBuilder
                .Entity<ForumTopic>()
                .HasData(
                    new ForumTopic
                    {
                        Id = 1,
                        ForumId = 1,
                        IsDeleted = false,
                        Subject = "Can you eat cats in this game?",
                        UserId = 3,
                    },
                    new ForumTopic
                    {
                        Id = 2,
                        ForumId = 1,
                        IsDeleted = false,
                        Subject = "You can skate on the ice with your backpack",
                        UserId = 4,
                    },
                    new ForumTopic
                    {
                        Id = 3,
                        ForumId = 2,
                        IsDeleted = false,
                        Subject = "Gravity fails when opening door",
                        UserId = 5,
                    },
                    new ForumTopic
                    {
                        Id = 4,
                        ForumId = 2,
                        IsDeleted = false,
                        Subject = "Grass turns pink after sleeping on floor",
                        UserId = 6,
                    });

            modelBuilder
                .Entity<ForumPost>()
                .HasData(
                    new ForumPost
                    {
                        Id = 1,
                        IsDelete = false,
                        ForumTopicId = 1,
                        Content = "You can, but in most cultures that is looked down upon. People's opinion of you will probably drop.",
                        UserId = 6,
                    },
                    new ForumPost
                    {
                        Id = 2,
                        IsDelete = false,
                        ForumTopicId = 1,
                        Content = "Can confirm. Ate cats. Got put on trial and hung for murder.",
                        UserId = 5,
                    },
                    new ForumPost
                    {
                        Id = 3,
                        IsDelete = false,
                        ForumTopicId = 2,
                        Content = "Now if I can only get my ox to pull me",
                        UserId = 4,
                    },
                    new ForumPost
                    {
                        Id = 4,
                        IsDelete = false,
                        ForumTopicId = 3,
                        Content = "Whenever I open a door, my character can float over obstacles...",
                        UserId = 3,
                    },
                    new ForumPost
                    {
                        Id = 5,
                        IsDelete = false,
                        ForumTopicId = 3,
                        Content = "That doesn't happen on my machine",
                        UserId = 1,
                    },
                    new ForumPost
                    {
                        Id = 6,
                        IsDelete = false,
                        ForumTopicId = 4,
                        Content = "Whenever I sleep on the floor without any blanket or pillows, I wake up to the grass being colored pink?!",
                        UserId = 2,
                    },
                    new ForumPost
                    {
                        Id = 7,
                        IsDelete = false,
                        ForumTopicId = 4,
                        Content = "That means your character contracted pink eye. Can you confirm whether this is true?",
                        UserId = 1,
                    });

            modelBuilder
                .Entity<User>()
                .HasData(
                    new User()
                    {
                        Id = 1,
                        IsDeleted = false,
                        Username = "Exonto",
                        HashedPassword = _passwordService.Hash("P@ssw0rd1"),
                    },
                    new User()
                    {
                        Id = 2,
                        IsDeleted = false,
                        Username = "Musafa",
                        HashedPassword = _passwordService.Hash("P@ssw0rd2"),
                    },
                    new User()
                    {
                        Id = 3,
                        IsDeleted = false,
                        Username = "Heffertripe0",
                        HashedPassword = _passwordService.Hash("P@ssw0rd3"),
                    },
                    new User()
                    {
                        Id = 4,
                        IsDeleted = false,
                        Username = "Original_Rando",
                        HashedPassword = _passwordService.Hash("P@ssw0rd5"),
                    },
                    new User()
                    {
                        Id = 5,
                        IsDeleted = false,
                        Username = "TheTrueBob23",
                        HashedPassword = _passwordService.Hash("P@ssw0rd6"),
                    },
                    new User()
                    {
                        Id = 6,
                        IsDeleted = false,
                        Username = "McStickins",
                        HashedPassword = _passwordService.Hash("P@ssw0rd7"),
                    });
        }
    }
}
