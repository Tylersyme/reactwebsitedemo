import { Api } from "./apiConstants";
import { AuthenticationService } from "./authenticationService";

export const UserLoginService = {
	_userLoginEndpoint: "/login",

	tryLogin: (loginCredentials) =>
		Api.post(`${UserLoginService._userLoginEndpoint}/Authenticate`, loginCredentials)
			.then(response => {
				if (!response.ok)
					throw new Error();

				return response.json();
			})
			.then(data => {
				AuthenticationService.updateAuthenticationUser(data);

				return true;
			})
			.catch(error => false),
};