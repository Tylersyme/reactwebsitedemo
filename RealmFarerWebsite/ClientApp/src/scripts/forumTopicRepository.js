import { Api } from "./apiConstants";

export const ForumTopicRepository = {
	_forumTopicEndpoint: "/forumtopic",

	getAllInForum: (forumId) => {
		const urlSearchParameters = new URLSearchParams({
			forumId: forumId
		});

		return Api.getAsJson(`${ForumTopicRepository._forumTopicEndpoint}/GetTopics?${urlSearchParameters}`);
	},

	add: (forumTopic) =>
		Api.post(`${ForumTopicRepository._forumTopicEndpoint}/PostForumTopic`, forumTopic),
};