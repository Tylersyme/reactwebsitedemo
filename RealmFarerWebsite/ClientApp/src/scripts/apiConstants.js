export const Api = {
  baseUrl: "api",

  _getEndpointUrl: (endpoint) =>
    `${Api.baseUrl}${endpoint}`,

  get: (endpoint) =>
    fetch(Api._getEndpointUrl(endpoint), {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem("token")}`,
      }
    }),

  getAsJson: (endpoint) =>
    Api.get(endpoint)
      .then(response => response.json()),

  post: (endpoint, body) =>
    fetch(Api._getEndpointUrl(endpoint), {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    }),
};
