import { Api } from "./apiConstants";

export const ForumPostRepository = {
	_forumPostEndpoint: "/forumpost",

	getAllInForumTopic: (forumTopicId) => {
		const urlSearchParameters = new URLSearchParams({
			forumTopicId: forumTopicId
		});

		return Api.getAsJson(`${ForumPostRepository._forumPostEndpoint}/GetPosts?${urlSearchParameters}`);
	},

	add: (forumPost) =>
		Api.post(`${ForumPostRepository._forumPostEndpoint}/PostForumPost`, forumPost),
};