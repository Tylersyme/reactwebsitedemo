import * as jwt from "jsonwebtoken";

export const AuthenticationService = {
	_tokenLocalStorageKey: "token",
	_loggedInUserIdLocalStorageKey: "loggedInUserId",

	_getAuthenticationToken: () =>
		localStorage.getItem(AuthenticationService._tokenLocalStorageKey),

	getLoggedInUserId: () =>
		parseInt(localStorage.getItem(AuthenticationService._loggedInUserIdLocalStorageKey)),

	updateAuthenticationUser: (authenticationUser) => {
		console.log(authenticationUser);

		localStorage.setItem(AuthenticationService._tokenLocalStorageKey, authenticationUser.token);
		localStorage.setItem(AuthenticationService._loggedInUserIdLocalStorageKey, authenticationUser.userId);
	},

	isAuthenticated: () => {
		const token = AuthenticationService._getAuthenticationToken();

		if (!token)
			return false;

		const decodedToken = jwt.decode(token, { complete: true });

		if (!decodedToken)
			return false;

		const currentDateInSeconds = new Date().getTime() / 1000;

		return decodedToken.payload.exp > currentDateInSeconds;
	},
};