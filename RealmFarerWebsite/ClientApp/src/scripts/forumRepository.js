import { Api } from "./apiConstants";

export const ForumRepository = {
  _forumApiEndpoint: "/forum",

  getAll: () =>
    Api.getAsJson(ForumRepository._forumApiEndpoint)
};
