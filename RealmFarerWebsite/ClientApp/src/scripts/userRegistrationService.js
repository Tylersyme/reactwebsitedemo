import { Api } from "./apiConstants";

export const UserRegistrationService = {
	_userRegistrationEndpoint: "/register",

	tryRegisterNewUser: (registrationData) =>
		Api.post(`${UserRegistrationService._userRegistrationEndpoint}`, registrationData)
			.then(response => response.json())
			.then(data => data.wasSuccessful),
};