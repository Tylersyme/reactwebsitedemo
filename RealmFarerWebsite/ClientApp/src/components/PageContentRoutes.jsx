import React, { Component } from "react";
import ForumsPage from "./pages/ForumsPage";
import ForumTopicsPage from "./pages/ForumTopicsPage";
import ForumPostsPage from "./pages/ForumPostsPage";
import LoginRegisterPage from "./pages/LoginRegisterPage";
import HomePage from "./pages/HomePage";
import { Route } from "react-router-dom";
import PrivateRoute from "./PrivateRoute";

export default class PageContentRoutes extends Component {
	render() { 
		return (  
			<React.Fragment>
				<Route exact path="/" component={HomePage} />
				<PrivateRoute exact path="/forums" component={ForumsPage} />
				<PrivateRoute exact path="/forums/:forumId" component={ForumTopicsPage} />
				<PrivateRoute exact path="/forums/:forumId/:topicId" component={ForumPostsPage} />
				<Route exact path="/loginregister" component={LoginRegisterPage} />
			</React.Fragment>
		);
	}
}