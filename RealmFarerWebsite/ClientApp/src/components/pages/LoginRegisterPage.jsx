import React, { Component } from "react";
import UsernamePassword from "../login-register/UsernamePassword";
import { UserRegistrationService } from "../../scripts/userRegistrationService";
import { UserLoginService } from "../../scripts/userLoginService";
import { AuthenticationService } from "../../scripts/authenticationService";
import "./LoginRegisterPage.css";

export default class LoginRegisterPage extends Component {

	constructor(props) {
		super(props);

		this.state = {
			loginUsername: "",
			loginPassword: "",
			registerUsername: "",
			registerPassword: "",
			isShowLoginMessage: false,
			loginMessage: "",
			isShowRegisterMessage: false,
			registerMessage: "",
		};
	}

	handleLoginClicked = () => {
		const loginCredentials = {
			username: this.state.loginUsername,
			plainTextPassword: this.state.loginPassword,
		};

		UserLoginService
			.tryLogin(loginCredentials)
			.then(wasSuccessful => this.showLoginMessage(wasSuccessful));

			this.resetFields();
	};

	showLoginMessage = (wasSuccessful) =>
		this.setState({
			isShowLoginMessage: true,
			loginMessage: wasSuccessful
				? "Login Successful"
				: "Login Failed"
		});

	handleRegisterClicked = () => {
		const registrationData = {
			username: this.state.registerUsername,
			plainTextPassword: this.state.registerPassword,
		};

		UserRegistrationService
			.tryRegisterNewUser(registrationData)
			.then(wasSuccessful => this.showRegisterMessage(wasSuccessful));

			this.resetFields();
	};

	showRegisterMessage = (wasSuccessful) =>
		this.setState({
			isShowRegisterMessage: true,
			registerMessage: wasSuccessful
				? "Registration Successful"
				: "Registration Failed"
		});

	resetFields = () => 
		this.setState({ 
			loginUsername: "",
			loginPassword: "",
			registerUsername: "",
			registerPassword: "",
			isShowLoginMessage: false,
			isShowRegisterMessage: false,
		});

	handleLoginUsernameChanged = (newUsername) =>
		this.setState({ loginUsername: newUsername });

	handleRegisterUsernameChanged = (newUsername) =>
		this.setState({ registerUsername: newUsername });

	handleLoginPasswordChanged = (newPassword) =>
		this.setState({ loginPassword: newPassword });

	handleRegisterPasswordChanged = (newPassword) =>
		this.setState({ registerPassword: newPassword });

	render() { 
		return (
			<div id="container">
				<div>
					<UsernamePassword 
						name="Login" 
						usernameValue={this.state.loginUsername}
						passwordValue={this.state.loginPassword}
						onUsernameChange={this.handleLoginUsernameChanged}
						onPasswordChange={this.handleLoginPasswordChanged} 
						onSubmitClick={this.handleLoginClicked} 
						message={this.state.loginMessage}/>
				</div>
				<UsernamePassword 
					name="Register" 
					usernameValue={this.state.registerUsername}
					passwordValue={this.state.registerPassword}
					onUsernameChange={this.handleRegisterUsernameChanged}
					onPasswordChange={this.handleRegisterPasswordChanged}
					onSubmitClick={this.handleRegisterClicked} 
					message={this.state.registerMessage}/>
			</div>
		);
	}
}