import React, { Component } from "react";
import { ForumRepository } from "../../scripts/forumRepository";
import ForumLink from "../forum/ForumLink";

export default class ForumsPage extends Component {

	constructor(props) {
		super(props);

		this.state = { 
			isFetching: true,
			forumData: [],
		};
	}

	componentDidMount() {
		ForumRepository
			.getAll()
			.then(data => {
				console.log(data);
				this.updateForumDataState(data);

				this.setState({ isFetching: false })
			});
	}

	updateForumDataState = (forumData) => 
		this.setState({forumData: forumData});

	getForumLinkElements = () => this.state.isFetching
		? null
		: this.state.forumData.map((forumData, index) => <ForumLink key={index} data={forumData} />);

	render() { 
		return (
			<div id="forums-link-list">
				{ this.getForumLinkElements() }
			</div>
		);
	}
}