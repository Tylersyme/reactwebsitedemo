import React, { Component } from "react";
import { Link } from "react-router-dom";
import { ForumTopicRepository } from "../../scripts/forumTopicRepository";
import { ForumPostRepository } from "../../scripts/forumPostRepository";
import NewForumPost from "../forum/NewForumPost";
import { AuthenticationService } from "../../scripts/authenticationService";
import "./ForumTopicsPage.css";

export default class ForumTopicsPage extends Component {

	constructor(props) {
		super(props);

		this.state = {
			forumTopicData: [],
			topicSubject: "",
			postContent: ""
		};
	}

	componentDidMount() {
		const forumId = this.getForumId();

		ForumTopicRepository
			.getAllInForum(forumId)
			.then(data => this.updateForumTopicDataState(data))
			.then(() => console.log(this.state.forumTopicData));
	}

	getForumId = () => 
		parseInt(this.props.match.params.forumId);

	updateForumTopicDataState = (forumTopicData) =>
		this.setState({ forumTopicData: forumTopicData });

	getForumTopicRowElements = () =>
		this.state.forumTopicData.map((forumTopicData, index) => (
			<React.Fragment key={index}>
				<div>
					<Link to={`/forums/${this.getForumId()}/${forumTopicData.id}`} className="emphasized-link">
						{forumTopicData.subject}
					</Link>
				</div>
				<div>
					{forumTopicData.username}
				</div>
				<div>
					{forumTopicData.totalPosts}
				</div>
			</React.Fragment>
		));

	handleTopicSubjectChanged = (newTopicSubject) =>
		this.setState({ topicSubject: newTopicSubject });

	handlePostContentChanged = (newPostContent) =>
		this.setState({ postContent: newPostContent });

	handlePostClicked = () => {
		// Prevent posting empty content
		if (!this.state.postContent || !this.state.topicSubject)
			return;

		const loggedInUserId = AuthenticationService.getLoggedInUserId();

		const forumTopic = {
			forumId: this.getForumId(),
			userId: loggedInUserId,
			subject: this.state.topicSubject,
		};

		ForumTopicRepository
			.add(forumTopic)
			.then(response => response.json())
			// The data contains the database generated id for the forum topic
			.then(data => {
				const forumPost = {
					forumTopicId: data.id,
					userId: loggedInUserId,
					content: this.state.postContent,
				};

				// Create forum post on new topic and navigate to topic page
				ForumPostRepository.add(forumPost)
					.then(() => this.redirectToTopic(data.id));
			});
	};

	redirectToTopic = (forumTopicId) => {
		const path = `/forums/${this.getForumId().toString()}/${forumTopicId.toString()}`;

		this.props.history.push(path);
	};

	render() { 
		return ( 
			<React.Fragment>
				<div id="forum-topics-container">
					<div>Subject</div>
					<div>Started By</div>
					<div>Replies</div>
					{ this.getForumTopicRowElements() }
				</div>
				<NewForumPost 
					includeTopicSubject={true}
					topicSubjectValue={this.state.topicSubject}
					postContentValue={this.state.postContent}
					onTopicSubjectChange={this.handleTopicSubjectChanged}
					onPostContentChange={this.handlePostContentChanged}
					onPostClick={this.handlePostClicked}/>
			</React.Fragment>
		);
	}
}