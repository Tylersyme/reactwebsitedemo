import React, { Component } from "react";
import { ForumPostRepository } from "../../scripts/forumPostRepository";
import NewForumPost from "../forum/NewForumPost";
import { AuthenticationService } from "../../scripts/authenticationService";
import "./ForumPostsPage.css";

export default class ForumPostsPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      forumPostsData: [],
      postContent: "",
    };
  }

  componentDidMount() {
    this
      .fetchPostsAsJson()
      .then(data => this.updateForumPostsDataState(data));
  }

  updateForumPostsDataState = (forumPostsData) =>
    this.setState({ forumPostsData: forumPostsData });

  fetchPostsAsJson = () => {
    const forumTopicId = this.getForumTopicId();

    return ForumPostRepository
      .getAllInForumTopic(forumTopicId);
  };

  getForumTopicId = () => 
    parseInt(this.props.match.params.topicId);

  handlePostClicked = () => {
    // Prevent posting empty content
    if (!this.state.postContent)
      return;

    const forumPost = {
      forumTopicId: this.getForumTopicId(),
      userId: AuthenticationService.getLoggedInUserId(),
      content: this.state.postContent,
    };

    // Create post and refresh all posts
    ForumPostRepository
      .add(forumPost)
      .then(() => this.fetchPostsAsJson())
      .then(data => {
        this.updateForumPostsDataState(data);

        this.setState({ postContent: "" });
      });
  };

  handlePostContentChanged = (newContent) => 
    this.setState({ postContent: newContent });

  getForumPostElements = () =>
    this.state.forumPostsData.map((forumPostData, index) => (
      <React.Fragment key={index}>
        <div className="forum-post-username">{forumPostData.username}</div>
        <div className="forum-post-content">{forumPostData.content}</div>
      </React.Fragment>
    ));

  render() {
    return (
      <React.Fragment>
        <div>Author</div>
        <div id="forum-posts-container">
          { this.getForumPostElements() }
        </div>
        <hr />
        <NewForumPost 
          includeTopicSubject={false} 
          postContentValue={this.state.postContent}
          onPostContentChange={this.handlePostContentChanged}
          onPostClick={this.handlePostClicked} />
      </React.Fragment>
    );
  }
}
