import React, { Component } from "react";
import { Link, } from "react-router-dom";
import "./ForumLink.css";

const ForumLink = ({ data }) => (
	<div id="forum-container">
		<div id="title-description">
			<Link to={`/forums/${data.id}`} className="emphasized-link">{data.title}</Link>
			<div>{data.description}</div>
		</div>
	</div>
);

export default ForumLink;