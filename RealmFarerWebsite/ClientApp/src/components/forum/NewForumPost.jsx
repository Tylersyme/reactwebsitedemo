import React, { Component } from "react";
import "./NewForumPost.css";

export default class NewForumPost extends Component {

	// Only gets the topic subject if this post will also create a new topic
	getTopicSubject = () =>
		this.props.includeTopicSubject
			? <React.Fragment>
				Subject: <input 
					type="text" 
					value={this.props.postSubjectValue}
					onChange={this.handleTopicSubectChanged} />
			  </React.Fragment>
			: null;

	handlePostContentChanged = (event) =>
		this.props.onPostContentChange(event.target.value);

	handleTopicSubectChanged = (event) =>
		this.props.onTopicSubjectChange(event.target.value);

	render() { 
		return (
			<div id="new-forum-post-container">
				<div>
					{ this.getTopicSubject() }
				</div>
				<textarea 
					value={this.props.postContentValue} 
					onChange={this.handlePostContentChanged} 
					style={{ resize: "none", width: "500px", height: "350px" }} />
				<button className="new-forum-post-button"
					onClick={this.props.onPostClick}
					style={{ width: "40px", height: "25px" }}>
				Post
				</button>
			</div>
		);
	}
}