import React from "react";
import { Route, Redirect } from "react-router-dom";
import { AuthenticationService } from "../scripts/authenticationService";

const PrivateRoute = ({ component: Component, ...rest }) => (
	<Route {...rest} render={(props) => (
		AuthenticationService.isAuthenticated()
			? <Component {...props} />
			: <Redirect to="loginregister" />
	)}/>
)

export default PrivateRoute;