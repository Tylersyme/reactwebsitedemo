import React, { Component } from "react";
import "./UsernamePassword.css";

export default class UsernamePassword extends Component {

	handleUsernameChanged = (event) =>
		this.props.onUsernameChange(event.target.value);

	handlePasswordChanged = (event) =>
		this.props.onPasswordChange(event.target.value);

	getMessageElement = () =>
		this.props.message
			? <div>{ this.props.message }</div>
			: null;

	render() { 
		return ( 
			<div className="username-password-container">
				<div>
					{ this.props.name }
				</div>
				<div>
					Username: <input 
						type="text" 
						value={this.props.usernameValue} 
						onChange={this.handleUsernameChanged} />
				</div>
				<div>
					Password: <input 
						type="password"
						value={this.props.passwordValue} 
						onChange={this.handlePasswordChanged}/>
				</div>
				<button onClick={this.props.onSubmitClick} className="submitButton">{ this.props.name }</button>
				{ this.getMessageElement() }
			</div>
		);
	}
}