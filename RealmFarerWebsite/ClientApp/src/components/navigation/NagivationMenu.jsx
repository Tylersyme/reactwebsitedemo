import React, { Component } from "react";
import "./NavigationMenu.css";
import { Link } from "react-router-dom";

export default class NavigationMenu extends Component {
  render() {
    return (
      <div id="navigation-menu">
        <Link to="/" id="home-page-logo" className="navigation-menu-link">Astral Blend</Link>
        <Link to="/forums" className="navigation-menu-link">Forums</Link>
        <Link to="/loginregister" id="login-register-link" className="navigation-menu-link">Login/Register</Link>
      </div>
    );
  }
}
