import React, { Component } from "react";
import NavigationMenu from "./components/navigation/NagivationMenu";
import PageContentRoutes from "./components/PageContentRoutes";
import "./App.css";

export default class App extends Component {
  displayName = App.name;

  render() {
    return (
      <React.Fragment>
        <NavigationMenu />
        <div className="page-content">
          <PageContentRoutes />
        </div>
      </React.Fragment>
    );
  }
}
